/**
 * Created by p24 on 30.05.2017.
 */
'use strict';

const reporter = require('../../node_modules/cucumber-html-reporter/index');
const path = require('path');
const fs = require('fs-extra');
const find = require('find');
// var base64 = require('../../node_modules/base-64/base64');
const translator = require('../../report/translator/report_translator');
const language = 'en';
const htmlFiles = 'report/html/cucumber_report_bootstrap.html';

// var encodedText = base64.encode('asdasdasd');

    const {defineSupportCode} = require('cucumber');

    defineSupportCode(function({registerHandler}: any) {

        registerHandler('AfterFeatures', function(features: any, callback: any) {

            const theme = {
                bootstrap: 'bootstrap',
                hierarchy: 'hierarchy',
                foundation: 'foundation',
                simple: 'simple'
            };

            const outputDirectory = 'report/html';
            const jsonFile = 'report/json/cucumber_report.json';

            function removeReports() {
                const files = find.fileSync(/\.html/, outputDirectory);
                files.map(function(file: any) {
                    fs.unlinkSync(file);
                });
            }

            function getOptions(reportTheme: any) {
                return {
                    theme: reportTheme,
                    output: path.join(outputDirectory, 'cucumber_report_' + reportTheme + '.html'),
                    reportSuiteAsScenarios: true,
                    launchReport: true,
                    jsonFile: '',
                    brandTitle: 'Raport BDD aplikacji Alex',
                    metadata: {
                        'App Version': '0.3.2',
                        'Test Environment': 'STAGING',
                        'Browser': 'Chrome  54.0.2840.98',
                        'Platform': 'Windows 10',
                        'Parallel': 'Scenarios',
                        'Executed': 'Remote'
                    }
                };
            }

            function getJsonFileOptions(reportTheme: any) {
                const options = getOptions(reportTheme);
                options.jsonFile = jsonFile;
                return options;
            }

            function assertJsonFile() {
                reporter.generate(getJsonFileOptions(theme.hierarchy));
            }

            removeReports();
            assertJsonFile();
            translator.translate(language, htmlFiles);

            callback();
        });

    });

    export {};

