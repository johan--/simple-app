/**
 * Created by p24 on 30.05.2017.
 */

'use strict';

import {browser, element, by, By, $, $$, ExpectedConditions} from 'protractor';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const expect = chai.expect;

const {defineSupportCode} = require('cucumber');

defineSupportCode(({setDefaultTimeout}: any) => {
    setDefaultTimeout(30000);
});

defineSupportCode(function(context: any) {

    const Given = context.Given;
    const When = context.When;
    const Then = context.Then;

    Given('I am on the main page', function (callback: any) {
        // browser.ignoreSynchronization = true;
        browser.get('http://alex.local');
        callback();
    });

    When('I click the add advertisement button', function (callback: any) {
        callback();
    });

    Then('I should see the form to add an advertisement', function (callback: any) {
        const el = element(by.id('app-name'));
        el.getText().then(function (text) {
            console.log(text);
            callback();
        });
    });

});

export {};