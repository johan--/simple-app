// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

exports.config = {
    seleniumAddress: 'http://selenium-standalone-chrome:4444/wd/hub',
    baseUrl: 'http://alex.local',

    capabilities: {
        browserName:'chrome'
    },

    framework: 'custom',

    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
        './features/*.feature'
    ],
    cucumberOpts: {
        require: ['./features/step_definitions/*.ts'],
        tags: [],
        strict: false,
        format: ["pretty", "json:report/json/cucumber_report.json"],
        dryRun: false,
        compiler: ["ts:ts-node/register"]
    },

    onPrepare: function () {
        browser.manage().window().maximize();
    }
};
