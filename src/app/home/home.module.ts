import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import {
    TopMenuComponent,
} from '../shared';

const homeRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: '',
        component: HomeComponent
    }
]);

@NgModule({
    declarations: [
        TopMenuComponent,
        HomeComponent
    ],
    imports: [
        homeRouting,
    ],
    providers: []
})
export class HomeModule {}
