/**
 * Created by p24 on 16.04.2017.
 */
import { Component } from '@angular/core';
import './top-menu.html';

@Component({
    selector: 'top-menu',
    templateUrl: './top-menu.html'
})
export class TopMenuComponent {}