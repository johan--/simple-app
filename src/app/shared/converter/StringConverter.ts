/**
 * Created by p24 on 23.04.2017.
 */
export const StringConverter = (value: any) => {
    if (value === null || value === undefined || typeof value === 'string') {
        return value;
    }

    return value.toString();
};



