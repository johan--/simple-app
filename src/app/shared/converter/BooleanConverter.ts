/**
 * Created by p24 on 23.04.2017.
 */
export const BooleanConverter = (value: any) => {
    if (value === null || value === undefined || typeof value === 'boolean') {
        return value;
    }

    return value.toString() === 'true';
};