'use strict';

const HtmlWebpack = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractPlugin = new ExtractTextPlugin({
    filename: 'main.css'
});
const CleanWebpack = require('clean-webpack-plugin');

const rootDir = path.resolve(__dirname);

module.exports = {
    devServer: {
        contentBase: path.resolve(rootDir, 'dist'),
        port: 3000,
        host: '0.0.0.0',
        disableHostCheck: true,
        public: '172.24.0.1'
    },
    watchOptions: {
        poll: true
    },
    devtool: 'source-map',
    entry: {
        "bundle": "./src/webpack.main.js",
        "plugins": "./src/plugins.js"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].js",
        // publicPath: "/dist"
    },
    resolve: {
        extensions: [ '.js', '.ts' ]
    },
    module: {
        exprContextCritical: false,
        rules: [
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        },
                    },
                    { loader: 'extract-loader' },
                    { loader: 'html-loader' }
                ],
                exclude: path.resolve(__dirname, 'src/index.html')
            },
            {
                test: /\index.html$/,
                loader: ['html-loader']
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(scss|css)$/,
                use: extractPlugin.extract({
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.(jpg|png|gif|eot|ttf|svg|woff|woff2)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'img/',
                        publicPath: 'img/'
                    }
                }]
            },
        ],
    },
    plugins: [
        new CleanWebpack(['dist']),
        extractPlugin,
        new HtmlWebpack({
            template: 'src/index.html'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "Tether": 'tether'
        })
    ],
};